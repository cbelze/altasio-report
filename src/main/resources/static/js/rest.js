$(document).ready(function(){
	$("#loginButton").on('click',function(e){
		e.preventDefault();
    var user = {
        'user' : $("#username").val(),
        'clave': $("#password").val()
    };
		$.ajax({
        url: 'login',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(user),
        success: function(data){
            console.log(data)
            if(data.success){
                window.location.href='home'
            }
        }
    })
	});

});
