package com.altasio.altapim.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altasio.altapim.bean.Usuario;
import com.altasio.altapim.dao.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Usuario login(String email, String password) {
		List<Usuario> usuario = new ArrayList<Usuario>();
		usuario = usuarioRepository.findByUserAndClave(email, password);
		if(usuario != null && usuario.size() > 0){
			return usuario.get(0);
		}else{
			return null;
		}
		
	}

}
