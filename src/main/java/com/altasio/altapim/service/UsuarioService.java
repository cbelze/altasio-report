package com.altasio.altapim.service;

import com.altasio.altapim.bean.Usuario;

public interface UsuarioService {

	public Usuario login(String email, String password);
	
}
