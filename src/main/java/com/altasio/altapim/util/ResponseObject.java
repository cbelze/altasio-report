package com.altasio.altapim.util;

public class ResponseObject {

	private Object object;
	private boolean isSuccess;
	//Si el messageCode es 200 significa que la operación fue exitosa.
	private String messageCode;
	
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
	
}
