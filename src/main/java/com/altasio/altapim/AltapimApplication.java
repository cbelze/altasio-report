package com.altasio.altapim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AltapimApplication {

	public static void main(String[] args) {
		SpringApplication.run(AltapimApplication.class, args);
	}
}
