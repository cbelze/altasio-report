package com.altasio.altapim.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.altasio.altapim.bean.Usuario;

public interface UsuarioRepository extends Repository<Usuario, Long>{

	@Query("select a from Usuario a where email=?1 and clave=?2")
	public Usuario login(String email, String password);
	public List<Usuario> findByUserAndClave(String email, String password);
}
