package com.altasio.altapim.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProyectoController {

	@RequestMapping(value="/matchmodule/categorization/",method=RequestMethod.GET)
	private String showProjectCategorization(){
		return "/matchmodule/ModuleCategorization/index";
	}
	
	@RequestMapping(value="/matchmodule", method=RequestMethod.GET)
	private String showModuleMatch(){
		return "/matchmodule/matchmodule";
	}
}
