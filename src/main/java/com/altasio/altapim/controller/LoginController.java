package com.altasio.altapim.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.altasio.altapim.bean.Usuario;
import com.altasio.altapim.service.UsuarioService;
import com.altasio.altapim.util.ResponseObject;

@Controller
public class LoginController {

	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping("/")
	public String showLogin(){
		return "/index";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public ResponseObject login(@RequestBody Usuario usuarioRequest){
		Usuario usuario = new Usuario();
		ResponseObject response = new ResponseObject();
		usuario = usuarioService.login(usuarioRequest.getUser(), usuarioRequest.getClave());
		if(usuario!= null){
			response.setObject(usuario);
			response.setSuccess(true);
			response.setMessageCode("200");
		}else{
			response.setSuccess(false);
			response.setMessageCode("1");
		}
		return response;
	}
	
	@RequestMapping("/home")
	public String showHome(){
		return "/home";
	}
	
}
