package com.altasio.altapim.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="clients")
@PrimaryKeyJoinColumn(name="clientUID",referencedColumnName="personuid")
public class Usuario extends Persona {

	
	@Column(name="clientname")
	private String user;
	
	@Column(name="clientpass")
	private String clave;
	
	@Column(name="status")
	private String estado;

	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	
}